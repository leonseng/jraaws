# Setup
## Create new virtualenv
`virtualenv jraaws_env`

## Enter virtualenv and install required packages
`source jraaws_env/bin/activate`
`pip install jraaws/requirements.txt`

## Set up jraaws as a systemd service
Create unit file in `/lib/systemd/system/jraaws.service`

```
[Unit]
Description=Jinja rendering as a webservice
After=multi-user.target

[Service]
Type=idle
ExecStart={{ virtualenv }}/bin/python {{ base_dir }}/run.py

[Install]
WantedBy=multi-user.target
```

Reload systemctl
`systemctl daemon-reload`

Enable autostart on boot
`systemctl enable jraaws`

Start the service
`systemctl start jraaws`
